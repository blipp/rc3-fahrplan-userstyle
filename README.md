# User Style for a more readable rC3 Fahrplan

This style makes changes to the [web version of the rC3 Fahrplan](https://rc3.world/rc3/public_fahrplan/),
to make it more readable.

Can be used with the [Stylus Firefox Add-on](https://addons.mozilla.org/en-US/firefox/addon/styl-us/),
for example. For a detailed how to, see below.

Tested on Firefox desktop.

## Table of Contents:
- [Changes implemented by this stylesheet](#changes)
- [Alternative schedule websites and formats](#alternatives)
- [Step-by-step installation guide using the Stylus add-on in Firefox](#how-to)
- [License](#license)

## Screenshot

![The schedule website with this stylesheet](doc/rc3_style_result.png)

## Changes

- use a standard font
- no bold font
- no text transformation (upper case etc)
- use standard scrollbar style
- always display horizontal scrollbar at the bottom of the window, instead of only at the bottom of the page
- sticky table header (room names)

## Alternatives

There are also alternative locations of the schedule:

- [pretalx.com/rc3/schedule/](https://pretalx.com/rc3/schedule/) by [rixx](https://twitter.com/rixxtr/status/1342947083540688896)
- [schedule in XML format](http://data.c3voc.de/rC3/everything.schedule.xml) by VOC, for use in for example the [Giggity smartphone app](https://wilmer.gaa.st/main.php/giggity.html). [QR code for scanning by @kunsi](https://chaos.social/@kunsi/105423141927863096)

## How to

1. Install the [Stylus Firefox Add-on](https://addons.mozilla.org/en-US/firefox/addon/styl-us/)
2. Navigate to the [schedule website](https://rc3.world/rc3/public_fahrplan/)
3. Click the Stylus button in the toolbar. In the newly shown popup menu, look for the text “Write style for:”, and below it, click on the “this URL” part of the link.
   ![Stylus button clicked, menu shows](doc/rc3_style_1.png)
4. A new tab or window will open. In the left sidebar, under “Mozilla Format”, click the button “Import”
   ![Button “Import” below “Mozilla Format”](doc/rc3_style_2.png)
5. A new popup will be opened, with a large text area. Copy-paste the content of `style.css` into it. Click “Overwrite Style”.
6. The popup will have closed, and the main text area shows the code that you copied. In the left sidebar, above “Mozilla Format”, click “Save”.
   ![Button “Save” above “Mozilla Format” in the left sidebar](doc/rc3_style_3.png)
7. You can close the tab or window of Stylus. The schedule website should look different now.

## License

This work is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0).
